package attributes;

/**
 * Primary attribute class
 * for storing character primary attributes
 *
 * @author  Emil Stubsjøen
 * @version 1.0
 */

public class PrimaryAttribute {
    int strength, dexterity, intelligence, vitality;

    public PrimaryAttribute(int strength, int dexterity, int intelligence, int vitality){
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.vitality = vitality;
    }

    public void increaseStrength(int amount) {
        this.strength += amount;
    }

    public void increaseDexterity(int amount) { this.dexterity += amount; }

    public void increaseIntelligence(int amount) {
        this.intelligence += amount;
    }

    public void increaseVitality(int amount) {
        this.vitality += amount;
    }

    public int getStrength() {
        return this.strength;
    }

    public int getDexterity() {
        return this.dexterity;
    }

    public int getIntelligence() {
        return this.intelligence;
    }

    public int getVitality() {
        return this.vitality;
    }
    
}
