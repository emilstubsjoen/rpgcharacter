package characters;

import attributes.PrimaryAttribute;
import enums.Slot;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import items.Armor;
import items.Item;
import items.Weapon;

import java.util.Formatter;
import java.util.HashMap;

/**
 * Abstract character class that includes common
 * methods for different characters
 *
 * @author  Emil Stubsjøen
 * @version 1.0
 */

public abstract class Character {
    protected int level;
    protected int health;
    protected double totalDps;
    protected String name;
    protected HashMap<Slot, Item> equipment = new HashMap<Slot, Item>();
    protected PrimaryAttribute primaryAttribute;
    protected PrimaryAttribute totalAttribute;
    StringBuilder statistics = new StringBuilder();

    public Character(String name) {
        this.name = name;
        this.level = 1;
    }

    public String getName(){
        return this.name;
    }

    public int getLevel() {
        return level;
    }

    public double getTotalDPS() {
        return totalDps;
    }

    public PrimaryAttribute getPrimaryAttribute() {
        return this.primaryAttribute;
    }


    /**
     * Method for equipping a character
     * with a certain item (i.e. armor or weapon)
     *
     * @param item The item to equip the character with
     */
    public boolean equipCharacter(Item item) throws InvalidWeaponException, InvalidArmorException {
        if (item instanceof Weapon) {
            if ( this.level < item.getLevel() ) {
                throw new InvalidWeaponException("The level required for using this weapon is too high");
            } else {
                Weapon weapon = (Weapon) item;
                equipWeapon(weapon);
                this.equipment.put(item.getSlot(), weapon);
            }
        }else {
            if (this.level < item.getLevel()) {
                throw new InvalidArmorException("The level required for using this armour is too high");
            } else {
                Armor armor = (Armor) item;
                equipArmor(armor);
                this.equipment.put(item.getSlot(), armor);
                this.totalAttribute = calculateTotalAttributes(armor.getAttributes());
            }
        }
        updateCharacter();
        return true;
    }

    /**
     * Helper method used to calculate the
     * total attributes of a character. I.e.
     * character attributes + attributes from
     * equipped item (armor)
     *
     * @param itemAttributes The attributes of the equipped armor.
     */
    private PrimaryAttribute calculateTotalAttributes(PrimaryAttribute itemAttributes) {
        return new PrimaryAttribute(this.primaryAttribute.getStrength() + itemAttributes.getStrength(),
            this.primaryAttribute.getDexterity() + itemAttributes.getDexterity(),
            this.primaryAttribute.getIntelligence() + itemAttributes.getIntelligence(),
                this.primaryAttribute.getVitality() + itemAttributes.getVitality());
    }

    /**
     * Helper method used update
     * the health of a character
     */
    protected void updateHealth() {
        this.health = primaryAttribute.getVitality()*10;
    }

    /**
     * Helper method used update
     * the total DPS of a character
     */
    private void updateTotalDPS() {
        double multiplier = 1;
        if (equipment.get(Slot.WEAPON) != null) multiplier = equipment.get(Slot.WEAPON).getDps();
        this.totalDps = multiplier * getCharacterDPS();
    }

    /**
     * Helper method used update
     * the statistics of a character
     */
    private void updateStatistics(){
        statistics.setLength(0);
        Formatter fmt = new Formatter(statistics);
        fmt.format("Name: %s%n", getName());
        fmt.format("Level: %d%n", getLevel());
        fmt.format("Strength: %d%n", getPrimaryAttribute().getStrength());
        fmt.format("Dexterity: %d%n", getPrimaryAttribute().getDexterity());
        fmt.format("Intelligence: %d%n", getPrimaryAttribute().getIntelligence());
        fmt.format("Vitality: %d%n", getPrimaryAttribute().getVitality());
        fmt.format("DPS: %f%n", getTotalDPS());
    }

    /**
     * Method used update the character's
     * DPS, health, and statistics.
     */
    protected void updateCharacter(){
        updateTotalDPS();
        updateHealth();
        updateStatistics();
    }

    /**
     * Method used to display the
     * character's statistics
     */
    public String getStatistics() {
        return statistics.toString();
    }

    /**
     * Abstract method to get the DPS
     * of a specific character
     */
    abstract double getCharacterDPS();

    /**
     * Abstract method to level
     * up a specific character
     */
    abstract void levelUp();

    /**
     * Abstract method used to equip
     * armor for a specific character
     */
    abstract void equipArmor(Armor armour) throws InvalidArmorException;

    /**
     * Abstract method used to equip
     * weapon for a specific character
     */
    abstract void equipWeapon(Weapon weapon) throws InvalidArmorException, InvalidWeaponException;


}