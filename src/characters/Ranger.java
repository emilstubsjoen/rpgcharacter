package characters;

import attributes.PrimaryAttribute;
import enums.ArmorType;
import enums.WeaponType;
import exceptions.InvalidArmorException;
import items.Armor;
import items.Weapon;

/**
 * Ranger character class that includes
 * implementation of abstract methods
 * from the character class
 *
 * @author  Emil Stubsjøen
 * @version 1.0
 */
public class Ranger extends Character {

    public Ranger(String name) {
        super(name);
        this.primaryAttribute = new PrimaryAttribute(1, 7, 1, 8);
        updateCharacter();
    }

    public void levelUp() {
        this.level++;
        this.primaryAttribute.increaseStrength(1);
        this.primaryAttribute.increaseDexterity(5);
        this.primaryAttribute.increaseIntelligence(1);
        this.primaryAttribute.increaseVitality(2);
    }

    @Override
    void equipArmor(Armor armor) throws InvalidArmorException {
        if ( ( armor.getArmorType() != ArmorType.LEATHER ) && ( armor.getArmorType() != ArmorType.MAIL ) ) {
            throw new InvalidArmorException("The armor " + armor.getArmorType()  + " is invalid for the character type " + toString());
        }
    }

    @Override
    void equipWeapon(Weapon weapon) throws InvalidArmorException {
        if ( weapon.getWeaponType() != WeaponType.BOW ) {
            throw new InvalidArmorException("The weapon " + weapon.getWeaponType()  + " is invalid for the character type " + toString());
        }
    }

    @Override
    public String toString(){
        return "RANGER";
    }

    @Override
    public double getCharacterDPS() {
        return (1 + (this.primaryAttribute.getDexterity()/100));
    }

}
