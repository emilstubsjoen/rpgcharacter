package characters;

import attributes.PrimaryAttribute;
import enums.ArmorType;
import enums.Slot;
import enums.WeaponType;
import exceptions.InvalidArmorException;
import items.Armor;
import items.Item;
import items.Weapon;

/**
 * Mage character class that includes
 * implementation of abstract methods
 * from the character class
 *
 * @author  Emil Stubsjøen
 * @version 1.0
 */

public class Mage extends Character{

    public Mage(String name){
        super(name);
        this.primaryAttribute = new PrimaryAttribute(1, 1, 8, 5);
        updateCharacter();
    }

    @Override
    public void levelUp(){
        this.level++;
        this.primaryAttribute.increaseStrength(1);
        this.primaryAttribute.increaseDexterity(1);
        this.primaryAttribute.increaseIntelligence(5);
        this.primaryAttribute.increaseVitality(3);
    }

    @Override
    void equipArmor(Armor armor) throws InvalidArmorException {
        if ( armor.getArmorType() != ArmorType.CLOTH) {
            throw new InvalidArmorException("The armor " + armor.getArmorType()  + " is invalid for the character type " + toString());
        }
    }

    @Override
    void equipWeapon(Weapon weapon) throws InvalidArmorException {
        if ( ( weapon.getWeaponType() != WeaponType.STAFF ) && ( weapon.getWeaponType() != WeaponType.WAND ) ) {
            throw new InvalidArmorException("The weapon " + weapon.getWeaponType()  + " is invalid for the character type " + toString());
        }
    }

    @Override
    public String toString(){
        return "MAGE";
    }

    @Override
    public double getCharacterDPS() {
        return (1 + (this.primaryAttribute.getIntelligence()/100));
    }

}
