package characters;

import attributes.PrimaryAttribute;
import enums.ArmorType;
import enums.WeaponType;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import items.Armor;
import items.Weapon;

/**
 * Warrior character class that includes
 * implementation of abstract methods
 * from the character class
 *
 * @author  Emil Stubsjøen
 * @version 1.0
 */

public class Warrior extends Character{

    public Warrior(String name){
        super(name);
        this.primaryAttribute = new PrimaryAttribute(5, 2, 1, 10);
        updateCharacter();
    }

    public void levelUp(){
        this.level++;
        this.primaryAttribute.increaseStrength(3);
        this.primaryAttribute.increaseDexterity(2);
        this.primaryAttribute.increaseIntelligence(1);
        this.primaryAttribute.increaseVitality(5);
    }

    @Override
    void equipArmor(Armor armor) throws InvalidArmorException {
        if ( ( armor.getArmorType() != ArmorType.MAIL ) && ( armor.getArmorType() != ArmorType.PLATE ) ) {
            throw new InvalidArmorException("The armor " + armor.getArmorType()  + " is invalid for the character type " + toString());
        }
    }

    @Override
    void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if ((weapon.getWeaponType() != WeaponType.AXE) && (weapon.getWeaponType() != WeaponType.SWORD) && (weapon.getWeaponType() != WeaponType.HAMMER)) {
            throw new InvalidWeaponException("The weapon " + weapon.getWeaponType() + " is invalid for the character type " + toString());
        }
    }

    @Override
    public String toString(){
        return "WARRIOR";
    }

    @Override
    public double getCharacterDPS() {
        return  (1 + (this.primaryAttribute.getStrength()/100));
    }
    
}
