package items;

import attributes.PrimaryAttribute;
import enums.ArmorType;

/**
 * Armor item class
 *
 * @author  Emil Stubsjøen
 * @version 1.0
 */

public class Armor extends Item {
    private ArmorType armorType;
    private PrimaryAttribute primaryAttribute;

    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType){
        this.armorType = armorType;
    }

    public void setAttributes(PrimaryAttribute primaryAttribute){
        this.primaryAttribute = primaryAttribute;
    }

    public PrimaryAttribute getAttributes() {
        return primaryAttribute;
    }
}
