package items;

import attributes.PrimaryAttribute;
import enums.Slot;

/**
 * Abstract item class that includes common
 * methods for different items (i.e.
 * armor and weapon)
 *
 * @author  Emil Stubsjøen
 * @version 1.0
 */

public abstract class Item {
    private String name;
    private int level;
    private Slot slot;

    public void setName(String name) {
        this.name = name;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public Slot getSlot() {
        return slot;
    }

    public int getLevel() {
        return this.level;
    }

    public double getDps() {
        return 0;
    }
}
