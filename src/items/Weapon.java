package items;

import enums.WeaponType;

/**
 * Weapon item class
 *
 * @author  Emil Stubsjøen
 * @version 1.0
 */

public class Weapon extends Item{
    private int damage;
    private WeaponType weaponType;
    private double attackSpeed;

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setDamage(int damage){
        this.damage = damage;
    }

    public void setWeaponType(WeaponType weaponType){
        this.weaponType = weaponType;
    }

    public void setAttackSpeed(double attackSpeed){
        this.attackSpeed = attackSpeed;
    }

    public int getDamage() {
        return this.damage;
    }

    @Override
    public double getDps() {
        return damage*attackSpeed;
    }

    
}
