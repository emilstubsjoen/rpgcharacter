package exceptions;

/**
 * Class for InvalidWeaponException
 * used when character tries to equip
 * invalid weapon
 *
 * @author  Emil Stubsjøen
 * @version 1.0
 */

public class InvalidWeaponException extends Exception{
    public InvalidWeaponException(String errorMessage) {
        super(errorMessage);
    }
}
