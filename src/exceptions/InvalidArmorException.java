package exceptions;

/**
 * Class for InvalidArmorException
 * used when character tries to equip
 * invalid armor
 *
 * @author  Emil Stubsjøen
 * @version 1.0
 */

public class InvalidArmorException extends Exception{
    public InvalidArmorException(String errorMessage) {
        super(errorMessage);
    }
}
