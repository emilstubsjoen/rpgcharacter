package enums;

/**
 * Enum for WeaponType
 *
 * @author  Emil Stubsjøen
 * @version 1.0
 */

public enum WeaponType {
    AXE,
    BOW,
    DAGGER,
    HAMMER,
    STAFF,
    SWORD,
    WAND,
}
