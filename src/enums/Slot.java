package enums;

/**
 * Enum for Slot
 *
 * @author  Emil Stubsjøen
 * @version 1.0
 */
public enum Slot {
    HEAD, BODY, LEGS, WEAPON
}
