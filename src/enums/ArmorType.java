package enums;

/**
 * Enum for ArmorType
 *
 * @author  Emil Stubsjøen
 * @version 1.0
 */
public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
