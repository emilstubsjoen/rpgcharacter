# RPGCharacter

RPGCharacter game

## Features

### Characters
  - Characters having attributes which increase at different rates as the character gains level.
### Equipment
  - Equipment such as armor and weapons, that characters can equip. 
  - Equipped items alter the power of the character causing it to deal more damage or survive longer.
  - Certain characters can equip certain items. Custom exceptions will be thrown if a character tries to equip wrong items.
### Testing
 - Full test coverage of the functionality.
  