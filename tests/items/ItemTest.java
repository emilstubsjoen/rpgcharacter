package items;

import attributes.PrimaryAttribute;
import characters.Mage;
import characters.Warrior;
import enums.ArmorType;
import enums.Slot;
import enums.WeaponType;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {
    @Test
    void equip_invalidWeaponLevel_shouldReturnInvalidWeaponException() {
        // Arrange
        Weapon testAxe = new Weapon();
        testAxe.setName("Common Axe");
        testAxe.setLevel(2);
        testAxe.setSlot(Slot.WEAPON);
        testAxe.setWeaponType(WeaponType.AXE);
        testAxe.setDamage(7);
        testAxe.setAttackSpeed(1.1);

        Warrior testWarrior = new Warrior("Hans Gunn");

        // Act
        String expected = "The level required for using this weapon is too high";
        Throwable exception = assertThrows(InvalidWeaponException.class, () -> testWarrior.equipCharacter(testAxe));

        // Assert
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void equip_invalidArmorLevel_shouldReturnInvalidArmorException() {
        // Arrange
        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body");
        testPlateBody.setLevel(2);
        testPlateBody.setSlot(Slot.BODY);
        testPlateBody.setArmorType(ArmorType.PLATE);
        testPlateBody.setAttributes(new PrimaryAttribute(1, 0, 0, 2));

        Warrior testWarrior = new Warrior("Hans Gunn");

        // Act
        String expected = "The level required for using this armour is too high";
        Throwable exception = assertThrows(InvalidArmorException.class, () -> testWarrior.equipCharacter(testPlateBody));

        // Assert
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void equip_invalidWeaponType_shouldReturnInvalidWeaponException() {
        // Arrange
        Weapon testBow = new Weapon();
        testBow.setName("Common Bow");
        testBow.setLevel(1);
        testBow.setSlot(Slot.WEAPON);
        testBow.setWeaponType(WeaponType.BOW);
        testBow.setDamage(12);
        testBow.setAttackSpeed(0.8);

        Warrior testWarrior = new Warrior("Hans Gunn");

        // Act
        String expected = "The weapon " + WeaponType.BOW + " is invalid for the character type " + testWarrior;
        Throwable exception = assertThrows(InvalidWeaponException.class, () -> testWarrior.equipCharacter(testBow));

        // Assert
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void equip_invalidArmorType_shouldReturnInvalidArmorException() {
        // Arrange
        Armor testClothHead = new Armor();
        testClothHead.setName("Common Cloth Head Armor");
        testClothHead.setLevel(1);
        testClothHead.setSlot(Slot.HEAD);
        testClothHead.setArmorType(ArmorType.CLOTH);
        testClothHead.setAttributes(new PrimaryAttribute(0, 0, 5, 1));

        Warrior testWarrior = new Warrior("Hans Gunn");

        // Act
        String expected = "The armor " + ArmorType.CLOTH + " is invalid for the character type " + testWarrior;
        Throwable exception = assertThrows(InvalidArmorException.class, () -> testWarrior.equipCharacter(testClothHead));

        // Assert
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void equip_validWeaponType_shouldReturnTrue() throws InvalidArmorException, InvalidWeaponException {
        // Arrange
        Weapon testWand = new Weapon();
        testWand.setName("Common Wand");
        testWand.setLevel(1);
        testWand.setSlot(Slot.WEAPON);
        testWand.setWeaponType(WeaponType.WAND);
        testWand.setDamage(12);
        testWand.setAttackSpeed(1.1);

        Mage testMage = new Mage("Sarah Sinn");

        // Act
        boolean actual = testMage.equipCharacter(testWand);

        // Assert
        assertTrue(actual);
    }

    @Test
    void equip_validArmorType_shouldReturnTrue() throws InvalidArmorException, InvalidWeaponException {
        // Arrange
        Armor testClothHead = new Armor();
        testClothHead.setName("Common Cloth Head Armor");
        testClothHead.setLevel(1);
        testClothHead.setSlot(Slot.HEAD);
        testClothHead.setArmorType(ArmorType.CLOTH);
        testClothHead.setAttributes(new PrimaryAttribute(0, 0, 5, 1));

        Mage testMage = new Mage("Sarah Sinn");

        // Act
        boolean actual = testMage.equipCharacter(testClothHead);

        // Assert
        assertTrue(actual);
    }

    @Test
    void calculateDPS_noWeapon_shouldReturnCorrectDPS() {
        // Arrange
        Warrior testWarrior = new Warrior("Hans Gunn");

        // Act
        double expected = 1*(1 + (5 / 100));
        double actual = testWarrior.getTotalDPS();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void calculateDPS_validWeapon_shouldReturnCorrectDPS() throws InvalidWeaponException, InvalidArmorException {
        // Arrange
        Weapon testAxe = new Weapon();
        testAxe.setName("Common Axe");
        testAxe.setLevel(1);
        testAxe.setSlot(Slot.WEAPON);
        testAxe.setWeaponType(WeaponType.AXE);
        testAxe.setDamage(7);
        testAxe.setAttackSpeed(1.1);

        Warrior testWarrior = new Warrior("Hans Gunn");
        testWarrior.equipCharacter(testAxe);

        // Act
        double expected = (7 * 1.1)*(1 + (5 / 100));
        double actual = testWarrior.getTotalDPS();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void calculateDPS_validArmor_shouldReturnCorrectDPS() throws InvalidWeaponException, InvalidArmorException {
        // Arrange
        Weapon testAxe = new Weapon();
        testAxe.setName("Common Axe");
        testAxe.setLevel(1);
        testAxe.setSlot(Slot.WEAPON);
        testAxe.setWeaponType(WeaponType.AXE);
        testAxe.setDamage(7);
        testAxe.setAttackSpeed(1.1);

        Warrior testWarrior = new Warrior("Hans Gunn");
        testWarrior.equipCharacter(testAxe);

        // Act
        double expected = (7 * 1.1)*(1 + (5 / 100));
        double actual = testWarrior.getTotalDPS();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void calculateDPS_validArmorAndWeapon_shouldReturnCorrectDPS() throws InvalidWeaponException, InvalidArmorException {
        // Arrange
        Weapon testAxe = new Weapon();
        testAxe.setName("Common Axe");
        testAxe.setLevel(1);
        testAxe.setSlot(Slot.WEAPON);
        testAxe.setWeaponType(WeaponType.AXE);
        testAxe.setDamage(7);
        testAxe.setAttackSpeed(1.1);

        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body");
        testPlateBody.setLevel(1);
        testPlateBody.setSlot(Slot.BODY);
        testPlateBody.setArmorType(ArmorType.PLATE);
        testPlateBody.setAttributes(new PrimaryAttribute(1, 0, 0, 2));

        Warrior testWarrior = new Warrior("Hans Gunn");
        testWarrior.equipCharacter(testAxe);
        testWarrior.equipCharacter(testPlateBody);

        // Act
        double expected = (7 * 1.1) * (1 + ((5+1) / 100));
        double actual = testWarrior.getTotalDPS();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void calculateDPS_validArmorAndWeaponAndLevelUp_shouldReturnCorrectDPS() throws InvalidWeaponException, InvalidArmorException {
        // Arrange
        Weapon testAxe = new Weapon();
        testAxe.setName("Common Axe");
        testAxe.setLevel(1);
        testAxe.setSlot(Slot.WEAPON);
        testAxe.setWeaponType(WeaponType.AXE);
        testAxe.setDamage(7);
        testAxe.setAttackSpeed(1.1);

        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body");
        testPlateBody.setLevel(1);
        testPlateBody.setSlot(Slot.BODY);
        testPlateBody.setArmorType(ArmorType.PLATE);
        testPlateBody.setAttributes(new PrimaryAttribute(1, 0, 0, 2));

        Warrior testWarrior = new Warrior("Hans Gunn");
        testWarrior.equipCharacter(testAxe);
        testWarrior.equipCharacter(testPlateBody);

        // Act
        double expected = (7 * 1.1) * (1 + ((5+1) / 100));
        double actual = testWarrior.getTotalDPS();

        // Assert
        assertEquals(expected, actual);
    }

}