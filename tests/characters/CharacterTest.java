package characters;

import attributes.PrimaryAttribute;
import enums.Slot;
import enums.WeaponType;
import exceptions.InvalidWeaponException;
import items.Weapon;
import org.junit.jupiter.api.Test;
import org.w3c.dom.ranges.Range;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    @Test
    void characterCreation_checkLevel_shouldBeAtLevel1() {
        // Arrange
        Warrior testWarrior = new Warrior("Hans Gunn");

        // Act
        int expected = 1;
        int actual = testWarrior.getLevel();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void levelGain_checkLevel_shouldBeAtLevel2() {
        // Arrange
        Rogue testRogue = new Rogue("Keith Rom");
        testRogue.levelUp();

        // Act
        int expected = 2;
        int actual = testRogue.getLevel();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void defaultAttributesMage_checkAttributes_shouldReturnCorrectDefaultAttributes() {
        // Arrange
        Mage testMage = new Mage("Sarah Sinn");

        // Act
        int expectedStrength =  1;
        int expectedDexterity =  1;
        int expectedIntelligence =  8;
        int expectedVitality =  5;

        int actualStrength =  testMage.getPrimaryAttribute().getStrength();
        int actualDexterity =  testMage.getPrimaryAttribute().getDexterity();
        int actualIntelligence =  testMage.getPrimaryAttribute().getIntelligence();
        int actualVitality =  testMage.getPrimaryAttribute().getVitality();

        // Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedVitality, actualVitality);
    }

    @Test
    void defaultAttributesRanger_checkAttributes_shouldReturnCorrectDefaultAttributes() {
        // Arrange
        Ranger testRanger = new Ranger("Vidkun Brann");

        // Act
        int expectedStrength =  1;
        int expectedDexterity =  7;
        int expectedIntelligence =  1;
        int expectedVitality =  8;

        int actualStrength =  testRanger.getPrimaryAttribute().getStrength();
        int actualDexterity =  testRanger.getPrimaryAttribute().getDexterity();
        int actualIntelligence =  testRanger.getPrimaryAttribute().getIntelligence();
        int actualVitality =  testRanger.getPrimaryAttribute().getVitality();

        // Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedVitality, actualVitality);
    }

    @Test
    void defaultAttributesRogue_checkAttributes_shouldReturnCorrectDefaultAttributes() {
        // Arrange
        Rogue testRogue = new Rogue("Keith Rom");

        // Act
        int expectedStrength =  2;
        int expectedDexterity =  6;
        int expectedIntelligence =  1;
        int expectedVitality =  8;

        int actualStrength =  testRogue.getPrimaryAttribute().getStrength();
        int actualDexterity =  testRogue.getPrimaryAttribute().getDexterity();
        int actualIntelligence =  testRogue.getPrimaryAttribute().getIntelligence();
        int actualVitality =  testRogue.getPrimaryAttribute().getVitality();

        // Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedVitality, actualVitality);
    }

    @Test
    void defaultAttributesWarrior_checkAttributes_shouldReturnCorrectDefaultAttributes() {
        // Arrange
        Warrior testWarrior = new Warrior("Hans Gunn");

        // Act
        int expectedStrength =  5;
        int expectedDexterity =  2;
        int expectedIntelligence =  1;
        int expectedVitality =  10;

        int actualStrength =  testWarrior.getPrimaryAttribute().getStrength();
        int actualDexterity =  testWarrior.getPrimaryAttribute().getDexterity();
        int actualIntelligence =  testWarrior.getPrimaryAttribute().getIntelligence();
        int actualVitality =  testWarrior.getPrimaryAttribute().getVitality();

        // Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedVitality, actualVitality);
    }

    @Test
    void levelUpAttributesMage_checkAttributes_shouldReturnCorrectLevelUpAttributes() {
        // Arrange
        Mage testMage = new Mage("Sarah Sinn");
        testMage.levelUp();

        // Act
        int expectedStrength =  1 + 1;
        int expectedDexterity =  1 + 1;
        int expectedIntelligence =  8 + 5;
        int expectedVitality =  5 + 3;

        int actualStrength =  testMage.getPrimaryAttribute().getStrength();
        int actualDexterity =  testMage.getPrimaryAttribute().getDexterity();
        int actualIntelligence =  testMage.getPrimaryAttribute().getIntelligence();
        int actualVitality =  testMage.getPrimaryAttribute().getVitality();

        // Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedVitality, actualVitality);
    }

    @Test
    void levelUpAttributesRanger_checkAttributes_shouldReturnCorrectLevelUpAttributes() {
        // Arrange
        Ranger testRanger = new Ranger("Vidkun Brann");
        testRanger.levelUp();

        // Act
        int expectedStrength =  1 + 1;
        int expectedDexterity =  7 +  5;
        int expectedIntelligence =  1 + 1;
        int expectedVitality =  8 + 2;

        int actualStrength =  testRanger.getPrimaryAttribute().getStrength();
        int actualDexterity =  testRanger.getPrimaryAttribute().getDexterity();
        int actualIntelligence =  testRanger.getPrimaryAttribute().getIntelligence();
        int actualVitality =  testRanger.getPrimaryAttribute().getVitality();

        // Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedVitality, actualVitality);
    }

    @Test
    void levelUpAttributesRogue_checkAttributes_shouldReturnCorrectLevelUpAttributes() {
        // Arrange
        Rogue testRogue = new Rogue("Keith Rom");
        testRogue.levelUp();

        // Act
        int expectedStrength =  2 + 1;
        int expectedDexterity =  6 + 4;
        int expectedIntelligence =  1 + 1;
        int expectedVitality =  8 + 3;

        int actualStrength =  testRogue.getPrimaryAttribute().getStrength();
        int actualDexterity =  testRogue.getPrimaryAttribute().getDexterity();
        int actualIntelligence =  testRogue.getPrimaryAttribute().getIntelligence();
        int actualVitality =  testRogue.getPrimaryAttribute().getVitality();

        // Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedVitality, actualVitality);
    }

    @Test
    void levelUpAttributesWarrior_checkAttributes_shouldReturnCorrectLevelUpAttributes() {
        // Arrange
        Warrior testWarrior = new Warrior("Hans Gunn");
        testWarrior.levelUp();

        // Act
        int expectedStrength =  5 + 3;
        int expectedDexterity =  2 + 2;
        int expectedIntelligence =  1 + 1;
        int expectedVitality =  10 + 5;

        int actualStrength =  testWarrior.getPrimaryAttribute().getStrength();
        int actualDexterity =  testWarrior.getPrimaryAttribute().getDexterity();
        int actualIntelligence =  testWarrior.getPrimaryAttribute().getIntelligence();
        int actualVitality =  testWarrior.getPrimaryAttribute().getVitality();

        // Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedVitality, actualVitality);
    }

}